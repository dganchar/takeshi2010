import inject
from sqlalchemy import create_engine

from sqlalchemy.orm import sessionmaker

from example.models.base import BaseModel
from example.repo.foo_repo import FooRepo
from example.repo.i_foo import FooRepoInterface
from example.services.foo_rpc import FooRPC, RemoteService
from example.services.i_foo_rpc import FooRPCInterface, RemoteServiceInterface

Session = sessionmaker()


def _init_session():
    engine = create_engine('postgres://postgres:postgres@127.0.0.1:5432/example', echo=True)
    Session.configure(bind=engine)
    BaseModel.metadata.create_all(engine)

    return Session()


@inject.params(session=Session)
def _init_foo_repo(session):
    """
    see: https://github.com/ivankorobkov/python-inject#step-by-step-example
    """
    return FooRepo(session)


def _injector_config(binder):
    """
    initialization of all our services
    """
    binder.bind_to_constructor(Session, _init_session)
    binder.bind_to_constructor(FooRepoInterface, _init_foo_repo)
    binder.bind_to_constructor(FooRPCInterface, FooRPC())

    binder.bind(RemoteServiceInterface, RemoteService())


def configure():
    def config(binder):
        binder.install(_injector_config)
    inject.clear_and_configure(config)

configure()
