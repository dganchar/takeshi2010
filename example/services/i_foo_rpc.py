import abc


class FooRPCInterface(abc.ABC):

    @abc.abstractmethod
    def create_foo(self, remote_id: str) -> int:
        pass


class RemoteServiceInterface(abc.ABC):

    @abc.abstractmethod
    def get_by_foo_remote_id(self, remote_id: str) -> dict:
        pass
