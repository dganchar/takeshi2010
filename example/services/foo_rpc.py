import inject
from nameko.rpc import rpc

from example.models.foo import Foo
from example.repo.i_foo import FooRepoInterface
from example.services.i_foo_rpc import FooRPCInterface, RemoteServiceInterface


class FooRPC(FooRPCInterface):

    name = "foo_rpc"
    # inject configured instances
    # see: https://github.com/ivankorobkov/python-inject#step-by-step-example
    _foo_repo = inject.attr(FooRepoInterface)
    _remote_service = inject.attr(RemoteServiceInterface)

    @rpc
    def create_foo(self, remote_id: str):
        foo = Foo(remote_id=remote_id)
        data = self._remote_service.get_by_foo_remote_id(foo.id)
        # do with result all what you need - here is just an example
        foo.name = data['name']

        return self._foo_repo.save_foo(foo).name


class RemoteService(RemoteServiceInterface):

    def get_by_foo_remote_id(self, remote_id: str) -> dict:
        # let's imagine that is result from other service
        return {'name': 'takeshi2010'}
