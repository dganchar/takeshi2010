from example.models.base import BaseModel
from sqlalchemy import Column, Integer, String


class Foo(BaseModel):
    __tablename__ = 'foo'

    id = Column(Integer, primary_key=True)
    remote_id = Column(String)
    name = Column(String)
