import abc

from example.models.foo import Foo


class FooRepoInterface(abc.ABC):

    @abc.abstractmethod
    def save_foo(self, foo: Foo) -> Foo:
        pass
