from example.repo.i_foo import FooRepoInterface
from example.models.foo import Foo


class FooRepo(FooRepoInterface):

    def __init__(self, session=None) -> None:
        self._session = session

    def save_foo(self, foo: Foo) -> Foo:
        self._session.add(foo)
        self._session.commit()
        return foo
