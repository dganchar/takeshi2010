How to run
------------


Prepare env:

```bash
# cd {project_path}
$ virtualenv env --python={path_to_python3.6}
$ source env/bin/activate
$ sudo pip3 install pip -U
$ sudo pip3 install -r requirements.txt
$ docker-compose up -d
$ docker exec -ti takeshi2010_db_1 psql -h localhost -U postgres -c "CREATE DATABASE example;"
```

Run app:

```bash
$ nameko run app --config ./foobar.yaml
```

Call service using shell:

```bash
$ nameko shell --config ./foobar.yaml
$ n.rpc.foo_rpc.create_foo(remote_id='uid')
```